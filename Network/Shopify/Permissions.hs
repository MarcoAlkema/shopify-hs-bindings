module Network.Shopify.Permissions (
    ShopifyScopes(..), ShopifyAccessLevel(..)
  ) where

data ShopifyAccessLevel =
    ShopifyRead
  | ShopifyWrite -- Implies ShopifyRead for the scope.
  deriving (Eq)

data ShopifyScopes = 
    ScopeContent ShopifyAccessLevel
  | ScopeThemes ShopifyAccessLevel
  | ScopeProducts ShopifyAccessLevel
  | ScopeCustomers ShopifyAccessLevel
  | ScopeOrders ShopifyAccessLevel
  | ScopeScriptTags ShopifyAccessLevel
  | ScopeShipping ShopifyAccessLevel
  deriving (Eq)

instance Show ShopifyAccessLevel where
  show ShopifyRead = "read"
  show ShopifyWrite = "write"

instance Show ShopifyScopes where
  show (ScopeContent l) = show l ++ "_content"
  show (ScopeThemes l) = show l ++ "_themes"
  show (ScopeProducts l) = show l ++ "_products"
  show (ScopeCustomers l) = show l ++ "_customers"
  show (ScopeOrders l) = show l ++ "_orders"
  show (ScopeScriptTags l) = show l ++ "_script_tags"
  show (ScopeShipping l) = show l ++ "_shipping"
